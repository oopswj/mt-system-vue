import ajax from './ajax'

//登录
export const login = (userId,password)=>ajax('http://localhost:9527/user/api/admin/login',{userId,password},'POST');
//获取管理员信息
export const getAdminInfo = (adminId)=>ajax('http://localhost:9527/user/api/admin',{adminId});
//分配管理员
export const distributeAdmin = (cinemaId)=>ajax('http://localhost:9527/user/api/admin/distribute',{cinemaId},'POST');


//获取当前页用户
export const getCurrentPageUser = (currentPage,pageSize,key)=>ajax('http://localhost:9527/user/api/user/list',{currentPage,pageSize,key});
//上传图片到服务器
export const upLoadImg = (file)=>ajax('http://localhost:9527/upload/api/upload',file,'POST');
//更新用户信息
export const updateUserInfo = (userId,userName,avatar,sex,phone,sign)=>ajax('http://localhost:9527/user/api/user/admin/modify',
                            {userId,userName,avatar,sex,phone,sign},'POST');
export const resetPassword = (userId,password)=>ajax('http://localhost:9527/user/api/user/admin/modify',{userId,password},'POST');                       
//删除用户信息
export const deleteUserInfo = (userId)=>ajax('http://localhost:9527/user/api/user/delete',{userId},'POST');
//添加用户信息
export const addUserInfo = (userName,avatar,phone,password,sex,sign)=>ajax('http://localhost:9527/user/api/user/admin/add',
                            {userName,avatar,phone,password,sex,sign},'POST');

//获取电影类型标签
export const getTags =()=>ajax('http://localhost:9527/film/api/film/tags');
//获取当前页电影
export const getCurrentPageMovie = (currentPage,pageSize,filmName)=>ajax('http://localhost:9527/film/api/film/page',{currentPage,pageSize,filmName});
//更新电影信息
export const updateMovieInfo = (id,filmName,poster,director,leadingRole,duration,type,language,releaseTime,introduction)=>
ajax('http://localhost:9527/film/api/film/modify',{id,filmName,poster,director,leadingRole,duration,type,language,releaseTime,introduction},'POST');
//添加电影信息
export const addMovieInfo = (filmName,poster,director,leadingRole,duration,type,language,releaseTime,introduction)=>
ajax('http://localhost:9527/film/api/film/add',{filmName,poster,director,leadingRole,duration,type,language,releaseTime,introduction},'POST');
//上传图片到服务器
export const upLoadMovieImg = (formData)=>ajax('/api/admin/upLoadMovieImg',formData,'POST');
//删除电影信息
export const deleteMovieInfo = (movieId)=>ajax('http://localhost:9527/film/api/film/delete',{movieId},'POST');

//获取当前页影院
export const getCurrentPageCinema = (currentPage,pageSize,cinemaName)=>ajax('http://localhost:9527/cinema/api/cinema/page',{currentPage,pageSize,cinemaName});
//更新影院信息
export const updateCinemaInfo = (id,cinemaName,phone,address)=>ajax('http://localhost:9527/cinema/api/cinema/modify',{id,cinemaName,phone,address},'POST');
//删除影院信息
export const deleteCinemaInfo = (id)=>ajax('http://localhost:9527/cinema/api/cinema/delete',{id},'POST');
//添加影院信息
export const addCinemaInfo = (cinemaName,phone,address)=>ajax('http://localhost:9527/cinema/api/cinema/add',{cinemaName,phone,address},'POST');


//获取当前影院
export const getCurrentCinemaInfo = (cinemaId)=>ajax('http://localhost:9527/cinema/api/cinema/'+cinemaId);
//获取当前页影厅
export const getCurrentPageHall = (currentPage,pageSize,hallName,cinemaId)=>ajax('http://localhost:9527/cinema/api/hall/'+cinemaId,{currentPage,pageSize,hallName,cinemaId});
//删除影厅
export const deleteHall = (id)=>ajax('http://localhost:9527/cinema/api/hall/delete',{id},'POST');
//更新影厅信息
export const updateHallInfo = (id,hallName,hallType,maxRow,maxColumn)=>ajax('http://localhost:9527/cinema/api/hall/modify',{id,hallName,hallType,maxRow,maxColumn},'POST');
//添加影厅信息
export const addHallInfo = (cinemaId,hallName,hallType,maxRow,maxColumn)=>ajax('http://localhost:9527/cinema/api/hall/add',{cinemaId,hallName,hallType,maxRow,maxColumn},'POST');

//获取当前影厅的座位安排
export const getSeatInfo = (currentPage,pageSize,hallId)=>ajax('http://localhost:9527/cinema/api/seat/'+hallId,{currentPage,pageSize});
//获取当前影厅信息
export const getCurHallInfo = (hallId)=>ajax('http://localhost:9527/cinema/api/hall/detail',{hallId});
//修改座位状态
export const updateSeatInfo = (id,status)=>ajax('http://localhost:9527/cinema/api/seat/modify',{id,status},'POST');
//修改座位状态
export const deleteSeatInfo = (id)=>ajax('http://localhost:9527/cinema/api/seat/delete',{id},'POST');

//获取当前页排片
export const getCurrentPageMovieSchedule = (currentPage,pageSize,cinemaId,filmName)=>
ajax('http://localhost:9527/cinema/api/schedule/page',{currentPage,pageSize,cinemaId,filmName});
//获取所有电影(排片)
export const getAllMovie = ()=>ajax('http://localhost:9527/film/api/film/all');
//获取所有电影(排片)
export const getAllHall = (cinemaId)=>ajax('http://localhost:9527/cinema/api/hall/all',{cinemaId});

//获取所有影院
export const getHallByCinemaId = (cinemaId)=>ajax('/api/admin/getHallByCinemaId',{cinemaId});
//获取排片的某天的时间段安排
export const getHasScheduleDateTime = (cinemaId,hallName,showDate)=>ajax('/api/admin/getHasScheduleDateTime',{cinemaId,hallName,showDate});

//添加排片信息
export const addScheduleInfo = (filmId,hallId,cinemaId,scheduleDate,startTime,endTime,ticketPrice)=>
ajax('http://localhost:9527/cinema/api/schedule/add',{filmId,hallId,cinemaId,scheduleDate,startTime,endTime,ticketPrice},'POST');
//删除电影排片
export const deleteMovieSchedule = (id)=>ajax('http://localhost:9527/cinema/api/schedule/delete',{id},'POST');

//获取当前页订单
export const getCurrentPageOrder = (currentPage,pageSize,filmName,cinemaId)=>ajax('http://localhost:9527/order/api/order/page',{currentPage,pageSize,filmName,cinemaId});
//删除订单
export const deleteOrder = (orderId,scheduleId,orderSeatInfo)=>ajax('/api/admin/deleteOrder',{orderId,scheduleId,orderSeatInfo},'POST');





