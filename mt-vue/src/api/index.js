import ajax from './ajax'



//获取验证码
export const getPhoneCode = (phone)=>ajax('http://localhost:9527/user/api/user/message',{phone},'POST');
//手机登录
export const phoneLogin = (phone,code)=>ajax('http://localhost:9527/user/api/user//message/login',{phone,code},'POST');
//账号密码登录
export const pwdLogin = (phone,password)=>ajax('http://localhost:9527/user/api/user/login',{phone,password},'POST');

//获取用户信息
export const getUserInfo = ()=>ajax('http://localhost:9527/user/api/user');
//获取用户详细信息
export const getUserDetailInfo = ()=>ajax('http://localhost:9527/user/api/user/detail');
//更新用户头像
export const updateUserAvatar = (avatar)=>ajax('http://localhost:9527/user/api/user/modify',{avatar},'POST');
//更新用户名
export const updateUserName = (userName)=>ajax('http://localhost:9527/user/api/user/modify',{userName},'POST');
//更新用户性别
export const updateUserSex = (sex)=>ajax('http://localhost:9527/user/api/user/modify',{sex},'POST');
//更新用户生日
export const updateUserBirthday = (birthday)=>ajax('http://localhost:9527/user/api/user/modify',{birthday},'POST');
//更新用户签名
export const updateUserSign = (sign)=>ajax('http://localhost:9527/user/api/user/modify',{sign},'POST');
//更新用户密码
export const updateUserPassword = (oldPassword,newPassword)=>ajax('http://localhost:9527/user/api/user/modifyPassword',{oldPassword,newPassword},'POST');
//上传图片到服务器
export const upLoadImg = (formData)=>ajax('http://localhost:9527/upload/api/upload',formData,'POST');
//更新用户信息
//export const updateUserInfo = (userName,avatar,password,sex,sign,birthday)=>ajax('/api/updateUserInfo',{userName,avatar,password,sex,sign,birthday},'POST');


//获取电影列表
export const getMovieList = (currentPage,pageSize)=>ajax('http://localhost:9527/film/api/film',{currentPage,pageSize});

//获取将要上映电影列表
export const getRecentlyReleaseMovieList = ()=>ajax('http://localhost:9527/film/api/film/recentlyRelease');
//获取电影详情
export const getMovieDetail = (filmId)=>ajax('http://localhost:9527/film/api/film/detail',{filmId});

//游客获取电影详情
export const viewerGetMovieDetail = (filmId)=>ajax('http://localhost:9527/film/api/film/viewer/detail',{filmId});

//想看电影
export const wishMovie = (filmId,status)=>ajax('http://localhost:9527/film/api/film/expect',{filmId,status},'POST');
//判断是否已经评论过
export const checkUserIsComment = (filmId)=>ajax('http://localhost:9527/film/api/comment/check',{filmId});
//获取当前用户的评论
export const getUserComment = (filmId)=>ajax('http://localhost:9527/film/api/comment/myComment',{filmId});
//新增用户评论
export const updateUserComment = (filmId,score,content,commentDate)=>ajax('http://localhost:9527/film/api/comment/add',{filmId,score,content,commentDate},'POST');
//获取所有用户通过审核的评论
export const getAllUserPassComment = (movieId)=>ajax('/api/getAllUserPassComment',{movieId});
//更新点赞状态
export const changeLikeStatus = (commentId,likeStatus)=>ajax('http://localhost:9527/film/api/like/add',{commentId,likeStatus},'POST');


//获取电影列表
export const getCinemaList = ()=>ajax('http://localhost:9527/cinema/api/cinema/all');
//获取当前影院详情
export const getCurrentCinemaDetail = (cinemaId)=>ajax('http://localhost:9527/cinema/api/cinema/'+cinemaId);
//获取当前影院上线的电影
export const getCurrentCinemaMovie = (cinemaId)=>ajax('http://localhost:9527/cinema/api/cinema/films',{cinemaId});
//获取当前影院排片list
export const getCurrentCinemaMovieSchedule = (filmId,cinemaId,queryTime)=>ajax('http://localhost:9527/cinema/api/schedule/list',{filmId,cinemaId,queryTime});
//获取排片详情
export const getScheduleById = (scheduleId)=>ajax('http://localhost:9527/cinema/api/schedule/'+scheduleId);
//请求创建订单
export const createOrder = (userId,scheduleId,seatList)=>ajax('http://localhost:9527/order/api/order',{userId,scheduleId,seatList},'POST');
//取消订单
export const cancelOrder =(orderId)=>ajax('http://localhost:9527/order/api/order/cancel',{orderId},'POST');
//获取当前电影有排片的电影院
export const getCurrentMovieSchedule = (filmId,queryTime)=>ajax('http://localhost:9527/cinema/api/cinema',{filmId,queryTime});
//根据名字模糊匹配电影
export const matchMovieByName = (filmName)=>ajax('http://localhost:9527/film/api/film/search',{filmName});
//根据名字模糊匹配影院
export const matchCinemaByName = (cinemaName)=>ajax('http://localhost:9527/cinema/api/cinema/search',{cinemaName});

//用户支付
export const payOrder = (orderId,payment)=>ajax('http://localhost:9527/pay/api/pay',{orderId,payment});

//轮询订单支付状态
export const getOrderPayStatus = (orderId)=>ajax('http://localhost:9527/order/api/order/check/'+orderId);

//获取个人订单信息
export const getOrderByUserId = ()=>ajax('http://localhost:9527/order/api/order');

//获取个人想看电影
export const getWishMovieByUserId = ()=>ajax('/api/getWishMovieByUserId');
//获取个人评论的电影
export const getIsWatchedMovieByUserId = ()=>ajax('/api/getIsWatchedMovieByUserId');
