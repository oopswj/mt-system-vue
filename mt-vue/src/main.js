// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import cookies from 'vue-cookies'
import storage from  './api/myStorage'
import axios from 'axios'
//配置字体图标
import "@/common/css/style.css";
Vue.config.productionTip = false;
Vue.prototype.storage=storage;
Vue.use(cookies);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
router.afterEach((to,from,next) => {
  window.scrollTo(0,0);
});

axios.interceptors.request.use(function (config) {
  // Do something before request is sent
 // let token =window.localStorage.getItem("x-token");
  let token = storage.get("x-token");
   //storage.get("x-token")//zheyang na duiba ??
  if (token) {
    //console.log(token);
     config.headers['x-token'] = token;
      // config.headers.accessToken = token;    //将token放到请求头发送给服务器
      //这里经常搭配token使用，将token值配置到tokenkey中，将tokenkey放在请求头中
      // config.headers['accessToken'] = Token;
  }
  return config;
});